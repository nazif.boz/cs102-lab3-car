package cs102;

public class Car {
    private double  odometer;
    private String brand;
    private String color;
    private int gear;
    private double totalnumberOfHoursDriven;

    public Car(){
        odometer = 0;
        brand = "Renualt";
        color= "White";
        gear = 0;
        totalnumberOfHoursDriven=0.;

    }
    public Car(String b, String c){
        odometer = 0;
        brand = b;
        color=  c;
        gear = 0;
        totalnumberOfHoursDriven=0.;
    }

    public Car(String b){
        odometer = 0;
        brand = b;
        color= "White";
        gear = 0;
        totalnumberOfHoursDriven=0.;


    }

    public void IncrementGear(){

        gear+= 1;
    }
    public void decrementGear(){

        gear-= 1;
    }
     public void Drive(double numberOfHoursTravel , double KmTraveledPerHour){
        // 50 Km/h, drive the car for 1.5 hours, the distance you cover? 75km = 50 km/h * 1.5 hours==> d= v *t.
       totalnumberOfHoursDriven+=numberOfHoursTravel;
         odometer += numberOfHoursTravel * KmTraveledPerHour;

}
public double getOdometer(){
        return odometer;
}
public String getBrand(){
        return brand;
}
public  String getColor(){
        return color;
}
public void  setColor(String c){
color = c;
}
public int getGear(){
        return gear;
}
public double gettotalnumberOfHoursDriven(){
        return totalnumberOfHoursDriven;
}

public void display(){

        System.out.println("The "+ brand + " has "+ color + "."
        +"It has travelled "+ odometer + "km so far. "
        +"It is at gear "+ gear + "."
        +"The car has been driven "+ totalnumberOfHoursDriven + "hours so far");
}
public double getAverageSpeed(){
        return odometer/totalnumberOfHoursDriven;
}

}

